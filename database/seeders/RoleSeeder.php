<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Role;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Role::create([
            "id" => 1,
            "name" => "Administrator"
        ]);

        Role::create([
            "id" => 2,
            "name" => "Bank"
        ]);

        Role::create([
            "id" => 3,
            "name" => "Shop"
        ]);

        Role::create([
            "id" => 4,
            "name" => "User"
        ]);
    }
}