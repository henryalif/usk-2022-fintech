<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Balance;

class BalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Balance::create([
            "user_id" => 1,
            "balance" => 900000
        ]);

        Balance::create([
            "user_id" => 2,
            "balance" => 600000
        ]);

        Balance::create([
            "user_id" => 3,
            "balance" => 10000
        ]);

        Balance::create([
            "user_id" => 4,
            "balance" => 90000
        ]);
    }
}