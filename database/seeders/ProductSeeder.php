<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Product;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Product::create([
            "name" => "Pizza",
            "price" => "9000",
            "desc" => "A great Taste",
            "stock" => "1000"
        ]);

        Product::create([
            "name" => "Burger",
            "price" => "3500",
            "desc" => "A great Burger",
            "stock" => "1000"
        ]);

        Product::create([
            "name" => "Hotdog",
            "price" => "8000",
            "desc" => "A great Hotdog",
            "stock" => "1000"
        ]);

        Product::create([
            "name" => "Lemon Tea",
            "price" => "4000",
            "desc" => "A great Lemon Tea",
            "stock" => "1000"
        ]);

        Product::create([
            "name" => "Strawberry Ice",
            "price" => "7000",
            "desc" => "A great Strawberry Ice",
            "stock" => "1000"
        ]);
    }
}
