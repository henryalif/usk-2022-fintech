<?php
namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Transaction;

class TransactionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Transaction::create([
            "user_id" => 4,
            "product_id" => 1,
            "qty" => 1,
            "invoice_id" => null,
            "type" => 2,
            "status" => 1,
        ]);
    }
}
