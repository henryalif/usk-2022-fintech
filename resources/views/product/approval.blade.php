@extends('layouts.admin')

@section('main-content')
        <div class="section-header">
            <h3 class="page__heading">Canteen Approval</h3>
        </div>
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">
                        <table id="example" class="table table-striped mt-4" style="width:100%">
                            <thead class="text-center">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Invoice</th>
                                    <th>Status</th>
                                    <th>Detail</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @foreach ($shopping_by_invoices as $key => $shopping_by_invoice)
                                    @if ($shopping_by_invoice->status == 2 || $shopping_by_invoice->status == 3)
                                    <tr>
                                        <td>{{ $key++ }}</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">{{ $shopping_by_invoice->user->name }}</td>
                                        <td><button type="button" class="btn btn-sm btn-info"><strong>{{ $shopping_by_invoice->invoice_id }}</strong></td>
                                        {{-- <td>{{ $shopping_by_invoice->status == 2 ? "PENDING" : "COMPLETED"}}</td> --}}
                                        <td>
                                            @if ($shopping_by_invoice->status == 2 )
                                                <button type="button" class="btn btn-sm btn-warning">PENDING</button>
                                            @else
                                                <button type="button" class="btn btn-sm btn-success">COMPLETED</button>
                                            @endif
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#detail-{{ $shopping_by_invoice->invoice_id }}">
                                                Detail
                                            </button>
                                            <!-- Modal -->
                                            <div class="modal fade" id="detail-{{ $shopping_by_invoice->invoice_id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                <div class="modal-dialog">
                                                    <div class="modal-content">
                                                        <div class="modal-header">
                                                        <h5 class="modal-title" id="exampleModalLabel">Order #{{ $shopping_by_invoice->invoice_id }}</h5>
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <table class="table table-bordered table-responsive-xl">
                                                                <thead>
                                                                    <tr>
                                                                        <th>No.</th>
                                                                        <th>Menu</th>
                                                                        <th>Qty</th>
                                                                        <th>Price</th>
                                                                        <th>Total</th>
                                                                    </tr>
                                                                </thead>
                                                                <tbody>
                                                                    <?php
                                                                        $counter = 1;
                                                                        $total_price = 0;
                                                                    ?>
                                                                        @foreach ($shopping_submissions as $shopping_submission)
                                                                            @if ($shopping_submission->invoice_id == $shopping_by_invoice->invoice_id)
                                                                            <?php $total_price += $shopping_by_invoice->qty * $shopping_submission->product->price; ?>
                                                                            <tr>
                                                                                <td>{{ $counter++ }}</td>
                                                                                <td>{{ $shopping_submission->product->name }}</td>
                                                                                <td>{{ $shopping_submission->qty }}</td>
                                                                                <td><button type="button" class="btn btn-sm btn-secondary"><strong>Rp. {{ number_format($shopping_submission->product->price) }}</strong></td>
                                                                                <td><button type="button" class="btn btn-sm btn-danger"><strong>Rp. {{ number_format($shopping_submission->qty * $shopping_submission->product->price) }}</strong></td>
                                                                            </tr>
                                                                            @endif
                                                                        @endforeach
                                                                </tbody>
                                                            </table>
                                                            Total Transaction :
                                                            {{-- <Strong>Rp. {{ number_format($total_price) }}</Strong> --}}
                                                            <button type="button" class=" text-center btn btn-block btn-outline-primary disabled">Total transaction : <Strong>Rp. {{ number_format($total_price) }}</Strong></button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            @if ($shopping_by_invoice->status == 3)
                                                <a class="btn btn-success btn-md" href="{{ route("canteen.approval", ["invoice_id" => $shopping_by_invoice->invoice_id]) }}" role="button"><i class="fas fa-check-square"></i></a>
                                                <a class="btn btn-danger btn-md" href="{{ route("canteen.rejected", ["invoice_id" => $shopping_by_invoice->invoice_id]) }}" role="button"><i class="fas fa-trash-alt"></i></a>
                                            @else
                                                <button type="button" class="btn btn-block btn-outline-danger disabled">Waiting Payment</button>
                                            @endif
                                        </td>
                                    </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection
@section('script')
<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );
</script>
@endsection
