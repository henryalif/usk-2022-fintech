@extends('layouts.admin')

@section('main-content')

<section class="section">
    <div class="section-header mb-3">
        <div class="card card-statistic-1">
            <div class="card-wrap">
                <div class="card-body">Balance : <strong><button type="button" class="btn btn-sm btn-danger">Rp. {{ number_format($balances->balance) }}</strong></div>
            </div>
        </div>
    </div>
    <div class="section-body">
        @if (session('status'))
            <div class="alert alert-success" role="alert">
                {{ session('status') }}
            </div>
        @endif
        <div class="row d-flex justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    {{-- <div class="card-header">Checkout {{ count($carts) > 0 ? "#" . $carts[0]->invoice_id : "" }}</div> --}}
                    <div class="card-body" style="height: auto; overflow: auto">
                        <table id="example" class="table table-striped mt-4" style="width:100%">
                            <thead class="text-center">
                                <tr>
                                    <th>#</th>
                                    <th>Name</th>
                                    <th>Qty</th>
                                    <th>Price</th>
                                    <th><strong>Total</strong></th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @foreach ($checkouts as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->product->name }}</td>
                                        <td><button type="button" class="btn btn-sm btn-dark">{{ $item->qty }}</td>
                                        <td>Rp. {{ number_format($item->product->price) }}</td>
                                        <td><button type="button" class="btn btn-sm btn-primary"><strong>Rp. {{ number_format($item->product->price * $item->qty) }}</strong></td>
                                        <td>
                                            <a class="btn btn-danger btn-md" href="{{ route("checkout-page.delete", ["id" => $item->id]) }}" role="button"><i class="fas fa-trash-alt"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <br>
                        <button type="button" class=" text-center btn btn-block btn-outline-primary disabled mb-3">Total transaction : <strong>Rp. {{ number_format($total_checkout) }}</button>
                            @if ($balances->balance < $total_checkout)

                            @else
                            <a href="{{ route("pay") }}" class="btn btn-block btn-md btn-success">Pay!</a>

                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );
</script>
@endsection
