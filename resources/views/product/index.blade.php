@extends('layouts.admin')

@section('main-content')

<section class="section">
    <div class="section-header">
        <div class="card mb-3">
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <h3>Product Page</h3>
                    </div>
                    <div class="col text-right">
                        <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
                        Cart
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Your carts</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <table class="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Item</th>
                                                <th>Qty</th>
                                                <th>Price</th>
                                                <th>Total</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($carts as $cart)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td><button type="button" class="btn btn-sm btn-secondary">{{ $cart->product->name }}</button></td>
                                                <td><button type="button" class="btn btn-sm btn-circle btn-secondary">{{ $cart->qty }}</button></td>
                                                <td><Strong>Rp. {{ number_format($cart->product->price) }}</Strong></td>
                                                <td><button type="button" class="btn btn-sm btn-secondary"><Strong>Rp. {{ number_format($cart->product->price * $cart->qty) }}</Strong></button></td>
                                                <td>
                                                    <a class="btn btn-danger btn-md" href="{{ route("cart.delete", ["id" => $cart->id]) }}" role="button"><i class="fas fa-trash-alt"></i></a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <button type="button" class=" text-center btn btn-block btn-outline-primary disabled">Total transaction : <strong>Rp. {{ number_format($total_cart) }}</button>
                                    {{-- <p class="text-center mt-3 mb-3"></strong></p> --}}
                                    {{-- <h6><strong>Rp. 100.000</strong></h6> --}}
                                    <a href="{{ route("checkout-cart") }}" class="btn btn-success btn-block mt-3 mb-3">Checkout!</a>
                                </div>
                                {{-- <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div> --}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="section-body">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    @foreach ($products as $item)
                    <div class="col-md-2 mt-4">
                        <div class="card">
                            <div class="card-header text-center mb-0">
                                <h4 class="card-title"><strong>{{ $item->name }}</strong></h4>
                            </div>
                            <div class="card-body">
                                <p class="card-text mb-0">{{ $item->desc }}</p>
                                <h5 class="card-text mb-3"><strong>Rp. {{ number_format($item->price) }}</strong></h5>
                            @if ($item->stock < 1)
                                <button type="button" class="btn btn-block btn-danger btn-sm disabled">Product Sold Out!</button>
                            @else
                            <form action="{{ route("addtocart", ["id" => $item->id]) }}" method="POST">
                                @csrf
                                <div class="input-group mb-3">
                                    <input type="number" class="form-control" name="qty" placeholder="Item" aria-label="Item" aria-describedby="button-addon2" value="1">
                                    <input type="hidden" name="product_id" value="{{ $item->id }}">
                                    <button class="btn btn-outline-success" type="submit" id="button-addon2"><i class="fas fa-cart-plus"></i></button>
                                </div>
                            </form>
                            @endif
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
