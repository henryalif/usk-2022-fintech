@extends('layouts.admin')

@section('main-content')
<section class="section">
    <div class="section-header">
        <h3 class="page__heading">User Management</h3>
    </div>
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <div class="card">
            <div class="card">
                <div class="card-header">
                    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#addUser">
                        Add User
                    </button>
                    <div class="modal fade" id="addUser" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Add user</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <form method="POST" action="{{ route('user.add') }}">
                                        @csrf
                                            <div class="form-group">
                                                <label>Name</label>
                                                <input type="text" class="form-control" name="name" value="{{ old("name") }}" id="name">
                                            </div>
                                            <div class="form-group">
                                                <label>Email</label>
                                                <input type="text" class="form-control" name="email" value="{{ old("email") }}" id="email">
                                            </div>
                                            <div class="form-group">
                                                <label for="exampleFormControlSelect1">Role</label>
                                                <select class="form-control @error('role_id') is-invalid @enderror" id="role_id" name="role_id">
                                                    <option selected>Choose...</option>
                                                    <option value="1">Admin</option>
                                                    <option value="2">Bank Teller</option>
                                                    <option value="3">Canteen</option>
                                                    <option value="4">User</option>
                                                </select
                                                @error('role_id')
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                            </div>
                                            <div class="form-group">
                                                <input type="hidden" class="form-control" name="balance" id="balance" value="0">
                                            </div>
                                            <div class="form-group">
                                                <label>Password</label>
                                                <input type="password" class="form-control form-control-user" name="password" placeholder="{{ __('Password') }}" required>
                                            </div>

                                            <div class="form-group">
                                                <label>Password Confirmation</label>
                                                <input type="password" class="form-control form-control-user" name="password_confirmation" placeholder="{{ __('Confirm Password') }}" required>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary">Save changes</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-body">
                    <table id="example" class="table table-striped mt-4" style="width:100%">
                        <thead class="text-center">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Balance</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @foreach ($user as $item)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td><strong>{{ $item->name }}</strong></td>
                                <td>{{ $item->email }}</td>
                                <td>
                                    @if ($item->role_id == 1)
                                        <button type="button" class="btn btn-sm btn-primary">{{ $item->role->name }}
                                    @endif
                                    @if ($item->role_id == 2)
                                        <button type="button" class="btn btn-sm btn-info">{{ $item->role->name }}
                                    @endif
                                    @if ($item->role_id == 3)
                                        <button type="button" class="btn btn-sm btn-success">{{ $item->role->name }}
                                    @endif
                                    @if ($item->role_id == 4){{ $item->role->name }}
                                    @endif
                                </td>
                                {{-- <td><button type="button" class="btn btn-sm btn-danger">{{ $item->role }}</td> --}}
                                <td><strong>{{ number_format($item->balance->balance) }}</strong></td>
                                <td>
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#editUser{{ $item->id }}">
                                        <i class="fas fa-edit"></i>
                                    </button>
                                    <!-- Modal -->
                                    <div class="modal fade" id="editUser{{ $item->id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="exampleModalLabel">Edit user</h5>
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                            <div class="modal-body text-left">
                                                <form method="POST" action="{{ route('user.edit', ["id" => $item->id ]) }}">
                                                            @method("put")
                                                            @csrf
                                                                <div class="form-group">
                                                                    <label>Name</label>
                                                                    <input type="text" class="form-control" name="name" value="{{ $item->name }}" id="name">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label>Email</label>
                                                                    <input type="text" class="form-control" name="email" value="{{ $item->email }}" id="email">
                                                                </div>
                                                                <div class="form-group">
                                                                    <label for="exampleFormControlSelect1">Role</label>
                                                                    <select class="form-control" id="exampleFormControlSelect1" name="role_id" id="role_id">
                                                                        <option selected>Choose...</option>
                                                                        <option value="1" {{ $item->role_id == "1" ? "selected" : "" }}>Admin</option>
                                                                        <option value="2" {{ $item->role_id == "2" ? "selected" : "" }}>Bank Teller</option>
                                                                        <option value="3" {{ $item->role_id == "3" ? "selected" : "" }}>Canteen</option>
                                                                        <option value="4" {{ $item->role_id == "4" ? "selected" : "" }}>User</option>
                                                                    </select>
                                                                </div>
                                                                <div class="form-group">
                                                                    <input type="hidden" class="form-control" id="balance" name="balance" value="0" value="{{ $item->balance }}">
                                                                </div>
                                                            </div>
                                                            <div class="modal-footer">
                                                                {{-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> --}}
                                                                <button type="submit" class="btn btn-primary">Save changes</button>
                                                            </div>
                                                </form>
                                            </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <a class="btn btn-warning btn-md" href="{{ route("user.edit", ["id" => $item->id]) }}" role="button"><i class="fas fa-edit"></i></a> --}}

                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-danger" data-toggle="modal" data-target="#deleteModal{{ $item->id }}">
                                        <i class="fas fa-trash-alt"></i>
                                    </button>
                                        <!-- Modal -->
                                    <div class="modal fade" id="deleteModal{{ $item->id }}" tabindex="-1" aria-labelledby="deleteModalLabel" aria-hidden="true">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h5 class="modal-title" id="deleteModalLabel">Delete user</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                    </button>
                                                </div>
                                                <div class="modal-body text-left">
                                                    Select "YES" below if you are sure to delete {{ $item->name }}?
                                                </div>
                                                <div class="modal-footer">
                                                    {{-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">No</button> --}}
                                                    <a href="{{ route("user.delete", ["id" => $item->id]) }}" type="submit" class="btn btn-primary">Yes</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            </div>
        </div>
    </div>
</section>
@endsection

@section('script')
<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );
</script>
@endsection
