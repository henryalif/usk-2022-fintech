@extends('layouts.admin')

@section('main-content')

<section class="section">
    <div class="section-header">
        <h3 class="page__heading">Bank</h3>
    </div>
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="section-body">
        <div class="row">
            <div class="col-lg-8">
                <div class="card">
                    <div class="card-header">
                        <h3 class="text-left">Top-Up saldo</h3>
                    </div>
                    <div class="card-body">
                        <form action="{{ route("topup")}}" method="POST">
                            @csrf
                            <div class="mb-3">
                                <label for="exampleInputEmail1" class="form-label">Input Nominal</label>
                                <input type="number" class="form-control" id="exampleInputEmail1" aria-describedby="" name="qty">
                                <input type="hidden" name="type" value="1">
                                <div id="emailHelp" class="form-text">The funds will enter if it has been allowed by the bank.</div>
                            </div>
                            <button type="submit" class="btn btn-success">Top Up!</button>
                        </form>
                    </div>
                </div>
            </div>
            {{-- bawah ini adalah bagian saldo sebelah kanan --}}
            <div class="col-lg-4">
                <div class="card card-statistic-1">
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Balance :</h4>
                        </div>
                        <div class="card-body">
                            <button type="button" class="btn btn-sm btn-danger"><strong>Rp. {{ number_format($balances->balance) }}</strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
