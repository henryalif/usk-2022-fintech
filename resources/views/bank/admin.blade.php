@extends('layouts.admin')

@section('main-content')
<section class="section">
    <div class="section-header">
        <h3 class="page__heading">Bank Approval</h3>
    </div>
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="example" class="table table-striped mt-4" style="width:100%">
                        <thead class="text-center">
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Nominal</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @foreach ($topuprequest as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td><button type="button" class="btn btn-sm btn-danger">{{ $item->user->name }}</td>
                                    <td><button type="button" class="btn btn-sm btn-warning"><strong>Rp. {{ number_format($item->qty) }}</strong></td>
                                    <td>
                                        <a class="btn btn-success btn-md" href="{{ route("topup.approval", ["transaction_id" => $item->id]) }}" role="button"><i class="fas fa-check-square"></i></a>
                                        <a class="btn btn-danger btn-md" href="{{ route("topup.rejected", ["transaction_id" => $item->id]) }}" role="button"><i class="fas fa-times-circle"></i></a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );
</script>
@endsection
