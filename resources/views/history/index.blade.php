@extends('layouts.admin')

@section('main-content')

<section class="section">
    <div class="section-header">
        <h3 class="page__heading">History</h3>
    </div>
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="example" class="table table-striped mt-4" style="width:100%">
                        <thead class="text-center">
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Invoice</th>
                                <th>Status</th>
                                <th>Detail</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @foreach ($transaction as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ date('d-m-Y',strtotime($item->created_at)) }}</td>
                                    <td>
                                        @if ($item->type == 1)
                                            <button type="button" class="btn btn-sm btn-success">{{ $item->invoice_id }}</div>
                                        @else
                                            <button type="button" class="btn btn-sm btn-danger">{{ $item->invoice_id }}</div>
                                        @endif
                                        {{-- {{ $item->invoice_id }} --}}
                                    </td>
                                    {{-- <td>Top Up</td> --}}
                                    <td>
                                        @if ($item->status == 1 )
                                            <button type="button" class="btn btn-sm btn-light">On Cart</button>
                                        @endif
                                        @if ($item->status == 2 )
                                            <button type="button" class="btn btn-sm btn-secondary">Pending</button>
                                        @endif
                                        @if ($item->status == 3 )
                                            <button type="button" class="btn btn-sm btn-info">Completed</button>
                                        @endif
                                        @if ($item->status == 4 )
                                            <button type="button" class="btn btn-sm btn-primary">Finished</button>
                                        @endif
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-primary btn btn-sm" data-toggle="modal" data-target="#detail-{{ $item->invoice_id }}">
                                            Detail
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="detail-{{ $item->invoice_id }}" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                    <h5 class="modal-title" id="exampleModalLabel">Order #{{ $item->invoice_id }}</h5>
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                        <span aria-hidden="true">&times;</span>
                                                    </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table class="table table-bordered table-responsive-xl">
                                                            <thead>
                                                                <tr>
                                                                    <th>No.</th>
                                                                    <th>Order</th>
                                                                    <th>Qty</th>
                                                                    <th>Price</th>
                                                                    <th>Total</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody>
                                                                @foreach ($details as $detail)
                                                                @if ($detail->invoice_id == $item->invoice_id)
                                                                <tr>
                                                                    <td>{{ $loop->iteration }}</td>
                                                                    <td>
                                                                        @if ($detail->type == 1)
                                                                            <button type="button" class="btn btn-sm btn-success">TOP UP</div>
                                                                        @endif
                                                                        @if ($detail->type == 2)
                                                                            {{ $detail->product->name }}</div>
                                                                        @endif
                                                                    </td>
                                                                    <td>{{ number_format($detail->qty) }}</td>
                                                                    @if ($detail->type == 1)
                                                                    @else
                                                                    <td><strong>Rp. {{ number_format($detail->product->price) }}</strong></td>
                                                                    <td><button type="button" class="btn btn-sm btn-primary"><strong>Rp. {{ number_format($detail->qty * $detail->product->price) }}</strong></td>
                                                                    @endif
                                                                </tr>
                                                                @endif
                                                                @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );
</script>
@endsection
