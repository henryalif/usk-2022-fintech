@extends('layouts.admin')

@section('main-content')

<section class="section">
    <div class="section-header">
        <h3 class="page__heading">History</h3>
    </div>
    @if (session('status'))
        <div class="alert alert-success" role="alert">
            {{ session('status') }}
        </div>
    @endif
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-header">
                    <a class="btn btn-success" href="{{ route("export") }}" role="button"><i class="fa-solid fa-print"></i></a>
                </div>
                <div class="card-body">
                    <table id="example" class="table table-striped mt-4" style="width:100%">
                        <thead class="text-center">
                            <tr>
                                <th>#</th>
                                <th>Date</th>
                                <th>Name</th>
                                <th>Invoice</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            @foreach ($alltransaction as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ date('d-m-Y',strtotime($item->created_at)) }}</td>
                                    <td>{{ $item->user->name }}</td>
                                    <td>
                                        @if ($item->type == 1)
                                            <button type="button" class="btn btn-sm btn-success">{{ $item->invoice_id }}</div>
                                        @else
                                            <button type="button" class="btn btn-sm btn-danger">{{ $item->invoice_id }}</div>
                                        @endif
                                    </td>
                                    <td>
                                        @if ($item->status == 1 )
                                            <button type="button" class="btn btn-sm btn-light">On Cart</button>
                                        @endif
                                        @if ($item->status == 2 )
                                            <button type="button" class="btn btn-sm btn-secondary">Pending</button>
                                        @endif
                                        @if ($item->status == 3 )
                                            <button type="button" class="btn btn-sm btn-info">Completed</button>
                                        @endif
                                        @if ($item->status == 4 )
                                            <button type="button" class="btn btn-sm btn-primary">Finished</button>
                                        @endif
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection
@section('script')
<script>
    $(document).ready(function() {
    $('#example').DataTable();
    } );
</script>
@endsection
