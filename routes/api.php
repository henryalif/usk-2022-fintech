<?php

use App\Balance;
use App\Product;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::prefix("/user")->group( function() {
    Route::get("/", function(){
        $users = User::all();

        return response()->json([
            "status"    => 200,
            "data"      => $users,
            "message"   => "Success get user"
        ]);
    });


    Route::post("/add", function(Request $request){
        $user = User::create([
            "name" => $request->name,
            "email" => $request->email,
            "password" => Hash::make($request->password),
            "role_id"   => $request->role_id
        ]);

        if($user->role_id == $request->role_id){
            Balance::create([
                "user_id" => $user->id,
                "balance" => 0
            ]);
        }

        return response()->json([
            "status" => 200,
            "data"  => $user,
            "message" => "Success add data"
        ]);
    });

    Route::post("/delete/{id}", function($id){
        $user = User::find($id);

        $balance = Balance::where("user_id", $user->id)->first();
        $balance->delete();

        $user->delete();

        return response()->json([
            "message" => "Success delete data"
        ]);
    });

});

Route::prefix("/product")->group( function() {
    Route::get("/", function(){
        $product = Product::all();

        return response()->json([
            "status" => 200,
            "data"  => $product,
            "message" => "Success show data"
        ]);
    });

    Route::post("/add", function(Request $request){
        $product = Product::create([
            "name"      => $request->name,
            "desc"     => $request->desc,
            "stock"  => $request->stock,
            "price"   => $request->price
        ]);


        return response()->json([
            "status" => 200,
            "data"  => $product,
            "message" => "Success add data"
        ]);
    });
});
