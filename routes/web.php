<?php

use App\Http\Controllers\BankController;
use App\Http\Controllers\HistoryController;
use App\Http\Controllers\ShopController;
use App\Http\Controllers\UserController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::get('/profile', 'ProfileController@index')->name('profile');
Route::put('/profile', 'ProfileController@update')->name('profile.update');

Route::prefix('bank')->group(function () {
    Route::get('/', [BankController::class, 'index'])->name('bank');
    Route::post('/topup', [BankController::class, 'create'])->name('topup');

    Route::get('/bank-admin', [BankController::class, 'showingRequest'])->name('bank.admin')->middleware('Bank');
    Route::get('/topup-approval/{transaction_id}', [BankController::class, 'topupApproval'])->name('topup.approval')->middleware('Bank');
    Route::get('/topup-rejected/{transaction_id}', [BankController::class, 'topupRejected'])->name('topup.rejected')->middleware('Bank');
});

Route::prefix('product')->group(function () {
    Route::get('/', [ShopController::class, 'index'])->name('transaction');

    Route::post('/addtocart/{id}', [ShopController::class, 'addToCart'])->name('addtocart');
    Route::get('/product-delete/{id}', [ShopController::class, 'deleteCart'])->name('cart.delete');
    Route::get('/checkout-cart', [ShopController::class, 'checkoutcart'])->name('checkout-cart');
    Route::get('/checkout-page', [ShopController::class, 'checkoutpage'])->name('checkout-page');
    Route::get('/checkout-delete/{id}', [ShopController::class, 'checkoutpagedelete'])->name('checkout-page.delete');
    Route::get('/pay', [ShopController::class, 'pay'])->name('pay');

    Route::get('/canteen-approval/{invoice_id}', [ShopController::class, 'canteenApproval'])->name('canteen.approval')->middleware('Shop');
    Route::get('/canteen-rejected/{invoice_id}', [ShopController::class, 'canteenRejected'])->name('canteen.rejected')->middleware('Shop');
    Route::get('/approval', [ShopController::class, 'sellerPage'])->name('approval')->middleware('Shop');
    Route::get('/admin', [ShopController::class, 'productShow'])->name('admin')->middleware('Shop');
    Route::post('/add', [ShopController::class, 'addProduct'])->name('product.add')->middleware('Shop');
    Route::put('/edit{id}', [ShopController::class, 'editProduct'])->name('product.edit')->middleware('Shop');
    Route::get('/delete{id}', [ShopController::class, 'deleteProduct'])->name('product.delete')->middleware('Shop');
});

Route::prefix("/history")->group( function() {
    Route::get('/', [HistoryController::class, 'index'])->name('history');
    Route::get('/admin', [HistoryController::class, 'admin'])->name('history.admin');

    Route::get('export', [HistoryController::class, 'export'])->name("export");

    Route::post('/add', [HistoryController::class, 'create'])->name('history.add');
    Route::put('/edit/{id}', [HistoryController::class, 'edit'])->name('history.edit');
    Route::get('/delete/{id}', [HistoryController::class, 'destroy'])->name('history.delete');
});

Route::prefix("/user")->group( function() {
    Route::get('/', [UserController::class, 'index'])->name('user')->middleware('User');
    Route::post('/add', [UserController::class, 'create'])->name('user.add')->middleware('User');
    Route::put('/edit/{id}', [UserController::class, 'edit'])->name('user.edit')->middleware('User');
    Route::get('/delete/{id}', [UserController::class, 'destroy'])->name('user.delete')->middleware('User');
});