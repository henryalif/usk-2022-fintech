<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $table = "users";

    protected $fillable = [
        'name',
        'email',
        'password',
        'role_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function getFullNameAttribute()
    // {
    //     if (is_null($this->last_name)) {
    //         return "{$this->name}";
    //     }

    //     return "{$this->name} {$this->last_name}";
    // }

    // public function setPasswordAttribute($value)
    // {
    //     $this->attributes['password'] = bcrypt($value);
    // }

    public function balance(){
        return $this->hasOne(Balance::class);
    }

    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function rolelist()
    {
        return $this->hasMany(Transaction::class);
    }

    public function balancelist(){
        return $this->hasMany(Balance::class);
    }

    public function balancelisttest(){
        return $this->hasMany(Balance::class);
    }

    public function usernamelist()
    {
        return $this->hasMany(User::class);
    }
}