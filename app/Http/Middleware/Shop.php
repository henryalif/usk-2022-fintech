<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class Shop
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (Auth::user()->role_id == '1') {
            return $next($request);
        } if(Auth::user()->role_id == '2') {
            abort(403);
        } if(Auth::user()->role_id == '3') {
            return $next($request);
        } if(Auth::user()->role_id == '4') {
            abort(403);
        } else {
            abort(403);
        }
        // if(auth()->user()->role !== 1 || auth()->user()->role !== 3) abort(403);
        // return $next($request);
    }
}