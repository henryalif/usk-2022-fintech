<?php

namespace App\Http\Controllers;

use App\Balance;
use App\Product;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::count();
        $products = Product::count();
        $balances = Balance::sum('balance');
        $invoices = Transaction::sum('id');
        $widget = [
            'users' => $users,
            'products' => $products,
            'balances' => $balances,
            'invoices' => $invoices
            //...
        ];

        return view('home', compact('widget'));
    }
}