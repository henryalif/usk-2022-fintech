<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\HistoryExport;

class HistoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $details = Transaction::where("user_id", Auth::user()->id)->whereIn("type",[1,2])->get();

        $transaction = Transaction::where("user_id", Auth::user()->id)->whereIn("type",[1,2])->groupBy("invoice_id")->get();

        return view("history.index", [
            "details" => $details,
            "transaction" => $transaction
        ]);
    }

    public function admin()
    {
        $alldetails = Transaction::whereIn("type",[1,2])->get();
        // dd($alldetails);
        $alltransaction = Transaction::whereIn("type",[1,2])->groupBy("invoice_id")->get();
        // dd($alltransaction);

        return view("history.admin", [
            "alldetails" => $alldetails,
            "alltransaction" => $alltransaction
        ]);
    }

    public function export()
    {
        return Excel::download(new HistoryExport, 'history.xlsx');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}