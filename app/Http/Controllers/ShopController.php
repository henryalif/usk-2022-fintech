<?php

namespace App\Http\Controllers;

use App\Balance;
use App\Product;
use App\Transaction;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ShopController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products   = Product::all(); //GET DATA PRODUCT
        $carts      = Transaction::where("user_id", Auth::user()->id)->where("status", 1)->where("type", 2)->get(); //VARIABLE CARTS UNTUK DIFOREACH DAN SET TYPE SERTA STATUS
        $checkouts  = Transaction::where("user_id", Auth::user()->id)->where("status", 2)->where("type", 2)->get(); //VARIABLE UNTUK CHECKOUTS DAN UPDATE STATUS MENJADI 2
        $balances   = Balance::where("user_id", Auth::user()->id)->first(); // MENGAMBIL BALANCE USER

        $total_cart = 0;
        $total_checkout = 0;

        foreach($carts as $cart){
            $total_cart +=($cart->product->price * $cart->qty);
        }

        foreach($checkouts as $checkout){
            $total_checkout +=($checkout->product->price * $checkout->qty);
        }

        return view("product.index",[
            "products"      => $products,
            "carts"         => $carts,
            "checkouts"     => $checkouts,
            "total_cart"    => $total_cart,
            "balances"      => $balances
        ],);
    }

    public function addToCart(Request $request, $id)
    {
        $product = Product::find($id); //FIND PRODUCT UNTUK MENDAPATKAN REQUEST ID

        if($product->stock < $request->qty)
            return back()->with("status", "Failed Add To Cart");

        Transaction::create([
            "user_id" => Auth::user()->id,
            "product_id" => $request->product_id,
            "status" => 1,
            "qty" => $request->qty,
            "type" => 2
        ]);

        $product->update([
            $product->stock = $product->stock - $request->qty
        ]);

        return redirect()->back()->with("status", "Success Add To Cart");
    }

    public function deleteCart($id)
    {
        $product = Transaction::find($id);
        Transaction::where("status", 1)->first();
        $product->delete();

        return back()->with("status", "Success Delete Cart!");
    }

    public function checkoutCart()
    {
        $invoice_id = "INV_" . Auth::user()->id . now()->timestamp;

        Transaction::where("user_id", Auth::user()->id)->where("type", 2)->where("status", 1)->update([
            "invoice_id" => $invoice_id,
            "status" => 2
        ]);

        return redirect()->back()->with("status", "Success Checkouts");
    }

    public function checkoutPage()
    {
        $balances   = Balance::where("user_id", Auth::user()->id)->first();
        $checkouts  = Transaction::where("user_id", Auth::user()->id)->where("status", 2)->where("type", 2)->get();

        $total_checkout = 0;
        foreach($checkouts as $checkout){
            $total_checkout +=($checkout->product->price * $checkout->qty);
        }

        // dd($total_checkout);

        return view("product.checkout",[
            "balances"          => $balances,
            "checkouts"         => $checkouts,
            "total_checkout"    => $total_checkout
        ],);
    }

    public function checkoutpagedelete($id)
    {
        // $products   = Product::all(); //GET DATA PRODUCT
        $product = Transaction::find($id);
        $item = Product::find($product->product_id);
        Transaction::where("status", 2)->first();
        $product->delete();

        $item->update([
            "stock"=>$item->stock + $product->qty
        ]);
        return back()->with("status", "Success Delete !");
    }

    public function pay()
    {
        // $products   = Product::all(); //GET DATA PRODUCT
        // $user       = User::where("user_id", Auth::user()->id);z
        $datas      = Transaction::where("user_id", Auth::user()->id)->where("type", 2)->where("status", 2);
        $balances   = Balance::where("user_id", Auth::user()->id)->first();
        $total_data = 0;


        // if($datas  > $user->balances);
        // return with("status", "Failed To Pay! - Balance is not sufficient");

        foreach($datas->get() as $data){
            $total_data +=($data->product->price * $data->qty);
        }

        $balances->update([
            "balance" => $balances->balance - $total_data
        ]);

        $datas->update([
            "status" => 3
        ]);

        return redirect()->back()->with("status", "Success make a payment, waiting seller approval! Thank You");
    }

    public function canteenApproval($invoice_id)
    {
        $transactions = Transaction::where("invoice_id", $invoice_id);

        $total_data = 0;

        foreach($transactions->get() as $transaction){
            $total_data += ($transaction->qty * $transaction->product->price);
        }

        $transactions->update([
            "status" => 4 //transaction finished
        ]);

        return redirect()->back()->with("status", "Shopping Approved!");

    }

    public function canteenRejected($invoice_id)
    {
        // $balances = Balance::where("user_id");
        $transactions = Transaction::where("invoice_id", $invoice_id); //VARIABLE UNTUK MENGAMBIL INVOICE_ID
        // dd($transactions);
        $total_data = 0;

        foreach($transactions->get() as $data){
            $total_data +=($data->product->price * $data->qty);
        } //FOREACH UNTUK GET DATA TRANSAKSI KEMUDIAN MENJUMLAHKAN PRICE DAN QTY

        $wallet = Balance::where("user_id", $transactions->first()->user_id)->first(); //menaambil balance wallet by transaction
        $wallet->balance = $wallet->balance + $total_data; // menjumlahkan balance user dengan request total data + balance awal
        $wallet->update();

        $transactions->update([
            "status" => 5 //transaction rejected
        ]);

        return redirect()->back()->with("status", "Shopping Rejected!");
    }

    public function sellerPage()
    {
        $shopping_submissions = Transaction::where("type", 2)->get();
        $shopping_by_invoices = Transaction::where("type", 2)->groupBy("invoice_id")->get();
        return view("product.approval",[
            "shopping_submissions"  => $shopping_submissions,
            "shopping_by_invoices"  => $shopping_by_invoices,
        ],);
    }

    public function productShow()
    {
        $products = Product::all();
        return view("product.admin",[
            "products"  => $products,
        ],);
    }

    public function addProduct(Request $request)
    {
        Product::create($request->all());
        return redirect()->back()->with("status", "Success Add New Products!");
    }

    public function editProduct(Request $request, $id)
    {
        Product::find($id)->update($request->all());
        return redirect()->back()->with("status", "Success Edit Products!");
    }

    public function deleteProduct($id)
    {
        Product::find($id)->delete();
        return redirect()->back()->with("status", "Success Delete Products!");
    }

    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}