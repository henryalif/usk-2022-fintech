<?php

namespace App\Exports;

use App\Transaction;
use Maatwebsite\Excel\Concerns\FromCollection;

class HistoryExport implements FromCollection
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Transaction::all();
    }

    // public function view(): View
    // {
    //     return view('exports.invoices', [
    //         'invoices' => Transaction::all()
    //     ]);
    // }
}
