<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $fillable = [
        'name',
        'price',
        'stock',
        'desc',
    ];

    public function transaction()
    {
        return $this->hasMany(Transaction::class);
    }

    public function productdetails(){
        return $this->belongsTo(Transaction::class);
    }
    
}
