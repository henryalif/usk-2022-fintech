<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Balance extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'balance'
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }

    public function userlist(){
        return $this->belongsTo(User::class);
    }

        public function userlisttest(){
        return $this->hasMany(User::class);
    }
}