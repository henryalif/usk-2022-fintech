<?php

namespace App;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $fillable = [
        'user_id',
        'name'
    ];

    public function user()
    {
        return $this->hasOne(User::class);
    }

        public function userlist()
    {
        return $this->belongsTo(User::class);
    }
}

// Role =
    // 1 = Admin
    // 2 = Bank
    // 3 = Shop
    // 4 = User
